#include "Game.h"
#include "Logging.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

int main() {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
		
	long long allocPoint = 0;
	if (allocPoint) {
		_CrtSetBreakAlloc(allocPoint);
	}

	double* foobars = new double[1024];

	Logger::Init();

	Game* game = new Game();
	game->Run();
	delete game;

	Logger::Uninitialize();

	return 0;
}
